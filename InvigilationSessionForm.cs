using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamSchedulingSystem
{
    public partial class InvigilationSessionsForm : Form
    {
        private ExamWizard examWizard;
        private UIController _uic;
        private string ExamName;
        private string filterStr;
        private string gradeFilterStr = "";
        private string dateFilterStr = "";

        public InvigilationSessionsForm(string examName)
        {
			// some test line
			// change 2
			// change 3 within feat/test2
            InitializeComponent();
            ExamName = examName;
            _uic = new UIController(this);
            populateInvigilatorComboBox(examName);
            populateExamDates(examName);

            ModifyComponents();

            examWizard = new ExamWizard(examName);  // exam wizard will prepare the invigilation list model in its constructor

            if (HasInvigilationData())
            {
				examWizard.BuildInvigilationSessionsModelFromDGV();		
            }
            else
            {
                examWizard.CreateInvigilationSessionsModel();
                examWizard.PreAssignInvigilator();				
            }
			
			BindInvigilationSessionsData();		
        }


		// Start scheduling 
        private void btnSchedule_Click(object sender, EventArgs e)
        {
            DialogResult confirmScheduling = PromptSchedulingConfirmation();
			bool isSchedulingSuccess = false;
			
            if(ConfirmScheduling==DialogResult.Yes)
            {
				SetupSchedulingEnvironment();

				isSchedulingSuccess = PerformAndGetSchedulingResult();
				BindInvigilationSessionsData();
				PromptSchedulingResultAndDetails();

				if (isSchedulingSuccess) 
				{					
					ExportFinalStaffDuty();
				}
				else
				{
					PromptSuggestedSolutions();
				}

            }
        }
		
		private bool HasInvigilationData() {
			return DBManager.IsTableEmpty("InvigilationSession" + ExamName);
		}
			
		private void BindInvigilationSessionsData() {
			dgvInvigilationSessions.DataSource = examWizard.InvigilationSessionDataTable();
			examWizard.ClearInvigilationSessionDB(); // have to clean up the existing DB in order to synchronize the DGV and DB
			_uic.PopulateModelIntoDgvInvigilationSession(dgvInvigilationSessions, examWizard.GetInvigilationList());
			DBManager.SaveDataTableIntoDB((DataTable)dgvInvigilationSessions.DataSource);		 
		}
		
		private void SetupSchedulingEnvironment() {
			DBManager.SaveDataTableIntoDB((DataTable)dgvInvigilationSessions.DataSource);
			examWizard.BuildInvigilationSessionsModelFromDGV();
			examWizard.ClearStaffList();
			examWizard.InitiailzeStaffList();
			examWizard.AssignDayOffStaff();
			examWizard.AssignAllStaffDuty();
			examWizard.SetAvailableStaffToSessions();		
		}
		
		private bool PerformAndGetSchedulingResult() {
			int bufferTime = GetBufferTimeBetweenInvigilationSessions();
			int maxAllowance = GetMaxAllowanceTime();
			
			return examWizard.PerformScheduling(bufferTime, maxAllowance);
		}
		
		private int GetMaxAllowanceTime() {
			int maxAllowanceTime = 0;
			
			do
			{
			} while (int.TryParse(Prompt.ShowDialog("Maximum staff invigilation time allowance", "Allowance Time"), out maxAllowanceTime) == false);		

			return maxAllowanceTime;
		}
		
		private int GetBufferTimeBetweenInvigilationSessions() {
			int bufferTime = 0;
			
			do
			{
			} while (int.TryParse(Prompt.ShowDialog("Please enter buffer time (minutes) between invigilation session", "Buffer Time"), out bufferTime) == false);
			
			return bufferTime;
		}
		
		private DialogResult PromptSchedulingConfirmation() {
			return MessageBox.Show("Save the data and perform scheduling?", "Confirm you choice", MessageBoxButtons.YesNo);
		}
		
		private void PromptSchedulingResultAndDetails() {
			MessageBox.Show(examWizard.SchedulingStatus);			
		}
					
		private void PromptSuggestedSolutions() {			
			MessageBox.Show("Suggested solutions:\n1) Pre-assign some sessions to staff \n2) Add more loading of staff");
		}
		
		private void ExportFinalStaffDuty() {
			try
			{
				examWizard.ExportFinalStaffDuty(examWizard.ExamName);
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}			
		}
		
		// Other internal methods for this form (e.g. populating fields to drop down values) are omitted for demonstration purpose

    }
}
